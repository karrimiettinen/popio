# POPIO

Run python3.11 program and store its stdin & stdout in sequential order.

Usage: `popio python_file.py input.txt > output.txt`

See also `popio --help`.

