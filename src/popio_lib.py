from subprocess import Popen, PIPE
import asyncio
from typing import IO
import threading
import sys
from dataclasses import dataclass

lock_stdio = False
stdio = "" # stdout & stdin buffer

@dataclass
class PopioOptions:
    program_file: str = "main.py"
    """Program filepath"""
    input_file: str = "input.txt"
    """Input filepath"""
    delay: float = 1
    """Delay in seconds"""
    pyver: str = "python3.11"

def readFile(Filename: str) -> list[str]:
    try:
        Filehandle = open(Filename, 'r', encoding="UTF-8")
        Rows = Filehandle.readlines()
        Filehandle.close()
    except Exception as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    return Rows

def writeFile(Filename: str, Content: str) -> None:
    try:
        Filehandle = open(Filename, 'w', encoding="UTF-8")
        Filehandle.write(Content)
        Filehandle.close()
    except Exception as err:
        print(err, file=sys.stderr)
        sys.exit(1)
    return None

async def feeder(stdin: IO[str], feed: str) -> None:
    global lock_stdio
    lock_stdio = True
    stdin.write(feed)
    stdin.flush()
    lock_stdio = False
    return None

def reader(stdout: IO[str]) -> None:
    global stdio, lock_stdio
    while True:
        if lock_stdio == False:
            character = stdout.read(1)
            if character == "":
                break # stdout complete
            print(character, end='')
            stdio += character
    return None

async def popio_lib(opts: PopioOptions) -> str:
    global stdio, lock_stdio
    feeds = readFile(opts.input_file)
    process = Popen([opts.pyver, "-u", opts.program_file],
                    stdout=PIPE,
                    stdin=PIPE,
                    text=True
                    )
    thread_read = threading.Thread(target=reader, args=(process.stdout,))
    thread_read.start()
    for feed in feeds: # user inputs
        await asyncio.sleep(opts.delay)
        lock_stdio = True
        print(feed, end='')
        stdio += feed # watchout
        lock_stdio = False
        asyncio.create_task(feeder(process.stdin, feed)) # thread_feed
    await asyncio.sleep(opts.delay) # Finishing wait
    return stdio