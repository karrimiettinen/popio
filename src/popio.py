import argparse
from pathlib import Path
from popio_lib import popio_lib, PopioOptions, writeFile
import asyncio
import sys

__version__ = "0.0.2"
"""POPIO version"""

class IArgs(PopioOptions):
    help: bool
    save: bool
    verbose: bool
    version: bool

def setFilepath(filepath: str) -> Path:
    print(filepath)
    path = Path(filepath)
    return path

async def main() -> None:
    global __version__
    argp = argparse.ArgumentParser(
        "POPI",
        description="Run python3.11 program and store its stdin & stdout in sequential order."
    )
    argp.add_argument("program_file", nargs='?', action="store", help="Python program filepath to run", default="main.py")
    argp.add_argument("input_file", nargs='?', action="store", default="input.txt", help="input filepath")
    argp.add_argument("-s", "--save", action="store", help="save file into filepath. Piping stdout also works.")
    argp.add_argument("-p", "--pyver", action="store", default="python3.11", help="Python version. By default python3.11. For Windows defaults to 'python'")
    argp.add_argument("-d", "--delay", type=float, action="store", default=1, help="Delay feeds (seconds - default 1s)")
    argp.add_argument("-v", "--verbose", action="store_true")
    argp.add_argument("-V", "--version", action="version", version=__version__)
    args: IArgs = argp.parse_args()
    if (sys.platform.startswith('win32')) or (sys.platform.startswith('cygwin')):
        args.pyver = "python"
    stdio = await popio_lib(PopioOptions(
        program_file=args.program_file,
        input_file=args.input_file,
        delay=float(args.delay),
        pyver=args.pyver
        ))
    if args.save:
        writeFile(args.save, stdio)
    return None

asyncio.run(main())
